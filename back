#!/usr/bin/env python
"""Back up a file or files."""
import os
import subprocess
import logging
from optparse import OptionParser

FORMAT = '%(levelname)s %(lineno)d %(message)s'
logging.basicConfig(format=FORMAT)

class File(object):

    """Given mappings and a file, figure everything out."""

    def __init__(self, mappings, push, pull, path, verbose=False):
        class Quiet(object):
            def __init__(self, quiet):
                self.q = quiet
            def __nonzero__(self):
                return self.q
            def __or__(self, other):
                if not self.q:
                    print other

        self.quiet = Quiet(not verbose)
        self._from_root = None
        assert mappings
        self.mappings = mappings
        assert not push or not pull
        self.push = push
        self.pull = pull
        self.path = path
        assert self.is_from or self.is_to, 'Cannot resolve path: %s' % path

    @property
    def from_root(self):
        """What is the path of the source's root."""
        if self._from_root is None:
            roots = set()
            if not self.pull:
                for root in self.mappings:
                    self.quiet | '%s starts with %s' % (self.full_path, root)
                    self.quiet | '%s starts with %s' % (self.path, root)
                    if self.full_path.startswith(root):
                        roots.add(root)
                        self.quiet | 'found %s' % root
                    if self.path.startswith(root):
                        roots.add(root)
                        self.quiet | 'found %s' % root
            if not self.push:
                for root, targets in self.mappings.items():
                    for target in targets:
                        self.quiet | '%s starts with %s' % (self.full_path,
                                                            target)
                        self.quiet | '%s starts with %s' % (self.path, target)
                        if self.full_path.startswith(target):
                            roots.add(root)
                            self.quiet | 'found %s' % root
                        if self.path.startswith(target):
                            roots.add(root)
                            self.quiet | 'found %s' % root
            assert roots, 'No root for %s in %s' % (self.path, self.mappings)
            self._from_root = max(roots, key=len)
        assert self._from_root
        return self._from_root

    @property
    def to_roots(self):
        """Where to send the file."""
        return self.mappings[self.from_root]

    @property
    def is_backed(self):
        """Has the file already been backed up."""
        return all(os.path.exists(to_path) for to_path in self.to_paths)

    @property
    def is_from(self):
        """Is the file in the source path."""
        self.quiet | '%s in %s' % (self.from_root, self.path)
        return not self.pull and self.from_root in self.path

    @property
    def is_to(self):
        """Is the file in a destination path."""
        self.quiet | 'any %s in %s' % (self.to_roots, self.path)
        return not self.push and (
            any(root in self.path for root in self.to_roots)
            or any(root in self.full_path for root in self.to_roots))


    @property
    def is_remote(self):
        return self.path.count(':') == 1 and not os.path.exists(self.path)

    @property
    def exists(self):
        """Does the file exist."""
        return self.is_remote or os.path.exists(self.path)

    @property
    def full_path(self):
        if self.is_remote:
            return self.path
        else:
            return os.path.abspath(self.path)

    @property
    def from_path(self):
        """Where is the file in the source path."""
        if self.is_from:
            path = self.full_path
        elif self.is_to:
            to_root = [x for x in self.to_roots
                       if self.path.startswith(x)
                       or self.full_path.startswith(x)][0]
            path = self.path.replace(to_root, self.from_root)

        if os.path.isdir(path):
            if '/' != path[-1]:
                path = path + '/'
        return path

    @property
    def to_paths(self):
        """Where is the file in the destination path."""
        return [self.from_path.replace(self.from_root, to_root)
                for to_root in self.to_roots]

    @property
    def is_dir(self):
        """Is the file a directory."""
        return self.exists and os.path.exists(self.from_path)

    def mkdir(self):
        """Create all directories in the dests as needed."""
        for to_path in self.to_paths:
            if self.is_backed:
                print 'Not creating dir for %s' % to_path
            else:
                if self.is_dir:
                    dirname = to_path
                else:
                    dirname = os.path.dirname(to_path)
                print 'Making dir: %s' % dirname
                if os.path.exists(dirname):
                    print 'Exists: %s' % dirname
                else:
                    print 'Creating %s' % dirname
                    os.makedirs(dirname)

    def backup(self):
        """rsync the file correctly."""
        # self.mkdir()
        for to_path in self.to_paths:
            assert self.from_path not in to_path, '%s in %s' % (
                self.from_path, to_path)
            command = ['rsync', '-rtv', '--progress', self.from_path, to_path]
            subprocess.call(command)

def get_configs(config_path=None):
    """Load src to dests mappings from ~/.back.ini"""
    if config_path is None:
        config_path = os.path.expanduser('~/.back.ini')
    from ConfigParser import SafeConfigParser
    config = {}
    parser = SafeConfigParser()
    parser.read(config_path)
    for section_name in parser.sections():
        config[section_name] = {}
        for name, value in parser.items(section_name):
            if 'dest' == name:
                value = [x.strip() for x in value.split(',')]
            config[section_name][name] = value
    return config

def main():
    """Do it."""
    parser = OptionParser()
    parser.add_option('-f', '--from', dest='from_root', default=None,
                      help='Use this as the source root')
    parser.add_option('-t', '--to', dest='to_root', default=None,
                      help='Use this as the backup root')
    parser.add_option('--push', dest='assume_from',
                      action='store_true', default=False,
                      help='Look in the sources for root of file')
    parser.add_option('--pull', dest='assume_to',
                      action='store_true', default=False,
                      help='Look in the destinations for root of file')
    parser.add_option('-v', '--verbose', dest='verbose',
                      action='store_true', default=False,
                      help='verbose is more verbose')
    (options, args) = parser.parse_args()
    options = options.__dict__
    if options.get('push') and options.get('pull'):
        raise ValueError('Do not specify push and pull together')
    if options.get('from_root') is not None:
        from_root = options.get('from_root')
        if not os.path.exists(from_root):
            raise ValueError('Path does not exist: {}'.format(from_root))
    if options.get('to') is not None:
        to_root = options.get('to')
        if not os.path.exists(to_root):
            raise ValueError('Path does not exist: {}'.format(to_root))
    verbose = options.get('verbose')
    configs = get_configs()
    mappings = {}
    for temp_root, config in configs.items():
        mappings[temp_root] = config.get('dest', [])
    from_root = options.get('from_root')
    to_root = options.get('to_root')
    if to_root is not None:
        to_roots = [x.strip() for x in to_root.split(',')]
    else:
        to_roots = []
    if from_root:
        if to_roots:
            mappings = {from_root: to_roots}
        elif from_root in mappings:
            mappings = {from_root: mappings[from_root]}
        else:
            raise ValueError(
                'No "to" available for files from {}'.format(from_root))
    if to_roots and not from_root:
        new_mappings = {}
        for from_root, candidate_to_roots in mappings.items():
            candidate_to_roots = list(set(to_roots).intersection(
                candidate_to_roots))
            if candidate_to_roots:
                new_mappings[from_root] = candidate_to_roots
        if not new_mappings:
            raise ValueError(
                'Could not find from for {}'.format(to_roots))
    assert(mappings)
    if len(args) < 1:
        print 'You must supply at least one path to back up'
    for arg in args:
        the_file = File(mappings, options.get('push'), options.get('pull'),
                        arg,
                        verbose=verbose)
        if verbose:
            print 'path %s' % the_file.path
            print 'exists %s' % the_file.exists
            print 'is_from %s' % the_file.is_from
            print 'is_to %s' % the_file.is_to
            print 'is_backed %s' % the_file.is_backed
            print 'from_path %s' % the_file.from_path
            print 'to_paths %s' % the_file.to_paths
        the_file.backup()

if __name__ == "__main__":
    main()
